#include "my_mat.h"

#include <stdio.h>

/**
 * Create shorests path matrix
 */
void loadShortestsPathMath(Mat mat, Mat dist)
{
    /**
     * the algoritem
     *
     * let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)
     * for each edge (u, v) do
     *  dist[u][v] ← w(u, v)  // The weight of the edge (u, v)
     * for each vertex v do
     *   dist[v][v] ← 0
     * for k from 1 to |V|
     *   for i from 1 to |V|
     *       for j from 1 to |V|
     *           if dist[i][j] > dist[i][k] + dist[k][j]
     *               dist[i][j] ← dist[i][k] + dist[k][j]
     *           end if
     */

    // fill the dist with -1
    for (size_t j = 0; j < MAT_LENGTH; j++)
    {
        for (size_t i = 0; i < MAT_LENGTH; i++)
        {
            if (i == j)
            {
                dist[j][i] = 0;
            }
            else if (mat[j][i] > 0)
            {
                dist[j][i] = mat[j][i];
            }
            else
            {
                dist[j][i] = -1;
            }
        }
    }

    // calc
    for (size_t k = 0; k < MAT_LENGTH; k++)
    {
        for (size_t j = 0; j < MAT_LENGTH; j++)
        {
            for (size_t i = 0; i < MAT_LENGTH; i++)
            {
                if (dist[j][k] != -1 && dist[k][i] != -1)
                {
                    if (dist[j][i] == -1 || dist[j][i] > dist[j][k] + dist[k][i])
                    {
                        dist[j][i] = dist[j][k] + dist[k][i];
                    }
                }
            }
        }
    }

    // cansel self paths
    /**
     * accoriding to the assigment empty group is not path
     */
    for (size_t i = 0; i < MAT_LENGTH; i++)
    {
        dist[i][i] = -1;
    }
}

/**
 * Return the 1 if exists path from i to j and 0 elsewize
 */
int existsPath(Mat dist, int i, int j)
{
    return (dist[j][i] == -1) ? 0 : 1;
}

/**
 * Return the wight of the shortests path from i to j
 * If the path dos not exists return -1
 */
int shortestsPath(Mat dist, int i, int j)
{
    return dist[j][i];
}
