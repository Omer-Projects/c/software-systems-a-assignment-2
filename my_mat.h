#pragma once

// Build Flags
#define DEBUG_MODE 0

// General Configs
#define MAT_LENGTH 10

typedef int Mat[MAT_LENGTH][MAT_LENGTH];

// algoritems
void loadShortestsPathMath(Mat mat, Mat dist);

int existsPath(Mat dist, int i, int j);

int shortestsPath(Mat dist, int i, int j);
