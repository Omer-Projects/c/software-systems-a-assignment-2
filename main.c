#include <stdio.h>
#include <stdlib.h>

#include "my_mat.h"

#define INPUT_MAX_LENGTH 1024

void get_mat(Mat mat)
{
    for (size_t i = 0; i < MAT_LENGTH; i++)
    {
        for (size_t j = 0; j < MAT_LENGTH; j++)
        {
            scanf("%d", &mat[j][i]);
        }
    }
}

// utils fpr development
void print_mat(Mat mat)
{
#if DEBUG_MODE
    for (size_t i = 0; i < MAT_LENGTH; i++)
    {
        printf("|");
        for (size_t j = 0; j < MAT_LENGTH; j++)
        {
            printf(" %d\t |", mat[j][i]);
        }
        printf("\n");
    }
    printf("\n");
#endif
}

int main()
{
    Mat mat;
    Mat dist;
    char action;

    scanf("%c", &action);
    while (action != 'D')
    {
        // do the action
        switch (action)
        {
        case 'A':
        {
            // get the matrix
            get_mat(mat);
            print_mat(mat);

            loadShortestsPathMath(mat, dist);

            print_mat(dist);
        }
        break;
        case 'B':
        {
            int i;
            int j;

            // get i and j
            scanf("%d %d", &i, &j);

            int result = existsPath(dist, i, j);

            if (result > 0)
            {
                printf("True\n");
            }
            else
            {
                printf("False\n");
            }
        }
        break;
        case 'C':
        {

            int i;
            int j;

            // get i and j
            scanf("%d %d", &i, &j);

            int result = shortestsPath(dist, i, j);
            printf("%d\n", result);
        }
        break;
        }

        scanf("%c", &action);
    }

    return 0;
}
