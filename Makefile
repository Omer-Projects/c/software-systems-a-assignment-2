# Project Path -
PROJECT_PATH=.

# Compiler prefix
CC=gcc -Wall

# clean, fixers and lintters
clean:
	rm -rf *.o connections

# code units
my_mat.o: $(PROJECT_PATH)/my_mat.c $(PROJECT_PATH)/my_mat.h
	$(CC) -c "$(PROJECT_PATH)/my_mat.c" -o my_mat.o

main.o: $(PROJECT_PATH)/main.c $(PROJECT_PATH)/my_mat.h
	$(CC) -c "$(PROJECT_PATH)/main.c" -o main.o

# applications
connections: main.o my_mat.o
	$(CC) my_mat.o main.o -o connections

# prod
all: connections

rebuild: clean all

# testing
test-1: rebuild
	./connections < "$(PROJECT_PATH)/inputs/input1.txt"

test-2: rebuild
	./connections < "$(PROJECT_PATH)/inputs/input2.txt"